#!/usr/bin/perl -w
#
# rotate-logs
#
# Copyright 2012 Steve McIntyre <93sam@debian.org>
#
# GPL v2+
#
# Helper script to rotate logs on wiki.debian.org
#
# For the logs from moin itself, we do a (slightly!) complicated dance
# to reduce the chance of losing any log data. When we touch the wsgi
# script in $moin_wsgi, apache will restart its moin instances. It
# will also create a new log file ($log) if needed and start logging
# there. So:
#
# 1. Rename the existing log file ($log) aside to a temporary location
#    ($log-TEMP); we will keep logging there for now
#
# 2. Touch the wsgi script to start a new log file ($log)
#
# 3. Work on the temporary file $log-TEMP, splitting as needed. The
#    code in split_old_log() will keep the most recent N months of log
#    data in $log-CURRENT
#
# 4. Delete $log-TEMP
#
# 5. If there is any data in $log:
#    a. move $log to $log-TEMP
#    b. touch the wsgi script
#    c. append $log-TEMP to $log-CURRENT
#    d. Delete $log-TEMP
#    e. iterate
#
# 6. Once we've found a lull (i.e. no data in $log):
#    a. move $log-CURRENT to $log
#    b. touch the wsgi script
#
# Doing this loop will minimse the chances of losing data as we rotate
# logs. It's *not* perfect, but it will do for our purposes. While
# we're in mid-rotate, things like RecentChanges and macros that parse
# the logs will return bogus data. There's not much we can do about
# that, short of making significant mods to Moin itself.
#
# Rotating our own local logs is simpler, thankfully, but we use much
# of the same code for consistency.
#
# This script does NO LOCKING yet; we don't expect it to run for so
# long that it's needed!

use strict;
use IO::Handle;
use File::stat;
use POSIX qw(mktime);

my $base = "/srv/wiki.debian.org";
my $moin_logdir = "$base/var/moin/data";
my $local_logdir = "$base/var/log";
my $moin_wsgi = "$base/bin/moin.wsgi";
my $file_mode = 0664; # We want newly-created working files to be group-writeable

my $keep_days = 90; # Minimum number of days of log data to keep
		    # around in current log files
my %accounts;
my $file_done = 0;
my $verbose = 0;
my $uid;
my $gid;

sub get_uid_by_name($)
{
    my $usernam = shift;
    my ($name,$passwd,$uid,$gid,
	$quota,$comment,$gcos,$dir,$shell,$expire) = getpwnam($usernam);
    if (defined($uid)) {
	return $uid;
    } else {
	die "Can't get uid for group $usernam!\n";
    }
}

sub get_gid_by_name($)
{
    my $groupnam = shift;
    my ($name, $passwd, $gid, $members) = getgrnam($groupnam);
    if (defined($gid)) {
	return $gid;
    } else {
	die "Can't get gid for group $groupnam!\n";
    }
}

sub get_cutoff_time()
{
    my $then = time() - ($keep_days * 86400);
    $then .= "000000";
    return $then;
}

sub convdate_hour($) {
    my $time = shift;
    $time =~ s/\.(\d)+$//g;
    if (length($time) == 16) {
        $time =~ s/\d\d\d\d\d\d$//;
    }
    my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = 
        gmtime($time);
    return sprintf("%4.4d-%2.2d-%2.2d %2.2d", $year+1900, $mon+1, $mday, $hour);
}

sub convmonth($) {
    my $time = shift;
    $time =~ s/\.(\d)+$//g;
    if (length($time) == 16) {
        $time =~ s/\d\d\d\d\d\d$//;
    }
    my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = 
        gmtime($time);
    return sprintf("%4.4d-%2.2d", $year+1900, $mon+1);
}

sub date_to_time_t($) {
    my $date = shift;
    if ($date =~ /(\d\d\d\d)-(\d\d)-(\d\d) (\d\d):(\d\d):(\d\d)/) {
	return mktime($6, $5, $4, $3, $2 - 1, $1 - 1900, 0, 0, 0) . "000000";
    }
}

sub restart_moin() {
    my ($atime, $mtime);
    my $file = $moin_wsgi;
    $atime = $mtime = time;
    utime $atime, $mtime, $file;
}

sub split_old_log($$$$) {
    my $in = shift;
    my $out = shift;
    my $pat = shift;
    my $logtype = shift;    

    my $out_month = "";
    my $num_lines_month = 0;
    my $num_lines_total = 0;
    my $cutoff_time = get_cutoff_time();
    my $after_cutoff = 0;
    my $out_filename = "";

    my ($fh_in, $fh_out);
    my $logdate = "";

    print "Splitting up $in:\n";
    open ($fh_in, "< $in") or die "Can't open $in for reading: $!\n";
    while (my $line = <$fh_in>) {
	my ($month, $hour);
	my $printed_hr = "";
	chomp $line;

	if ($logtype eq 'moin') {
	    if ($line =~ m/$pat/) {
		$logdate = $1;
	    } else {
		print "Ignoring malformed log line #" . $fh_in->input_line_number() . ": $line\n";
		next;
	    }
	} else {
	    if ($line =~ m/$pat/) {
		$logdate = date_to_time_t($1);
	    }
	}

	if (!$after_cutoff) {
	    $month = convmonth($logdate);
	    if (($logdate >= $cutoff_time) or ($month ne $out_month)) {
		if (length($out_month)) {
		    print "\r  $out_filename: $num_lines_month lines\n";
		    close($fh_out);
		}
		if ($logdate >= $cutoff_time) {
		    $after_cutoff = 1;
		    $out_filename = "$out-CURRENT";
		    open($fh_out, ">> $out_filename")
			or die "can't open $out_filename for writing: $!\n";
		    # Explicitly set up ownership and perms for
		    # -CURRENT; this will become the working logfile
		    # once we're done, so needs to work on the system
		    chown $uid, $gid, $out_filename;
		    chmod $file_mode, $out_filename;
		} else {
		    $out_month = $month;
		    $out_filename = "$out-$month.gz";
		    open($fh_out, "| gzip -9 >> $out_filename")
			or die "can't open $out_filename for writing: $!\n";
		    # We don't care about perms on the rotated files
		    # in the same way, so no chown/chmod
		}
		$num_lines_month = 0;
	    }	    
	}

	# Actually write the output line
	print $fh_out "$line\n";
	$num_lines_month++;
	$num_lines_total++;

	if ($verbose) {
	    $hour = convdate_hour($logdate);
	    if ($printed_hr ne $hour) {
		$printed_hr = $hour;
		print "\r  $printed_hr";
	    }
	}
    }
    close $fh_in;
    close $fh_out;
    print "\r  $out_filename: $num_lines_month lines\n";
    print "  $num_lines_total total\n";
}

sub rotate_log($$$) {
    my $infile = shift;
    my $pat = shift;
    my $logtype = shift;

    while (1) {
	if ((! -e $infile) or (-z $infile)) {
	    last;
	}
	unlink("$infile-TEMP");
	rename("$infile", "$infile-TEMP");
	if ($logtype eq "moin") {
	    restart_moin();
	}
	split_old_log("$infile-TEMP", "$infile", $pat, $logtype);
	unlink("$infile-TEMP");
	if ($logtype eq "moin") {
	    restart_moin();
	}
    }

    rename("$infile-CURRENT", "$infile");
    if ($logtype eq "moin") {
	restart_moin();
    }
}

$uid = get_uid_by_name("wiki");
$gid = get_gid_by_name("wikiweb");

chdir $moin_logdir or die "Can't cd to $moin_logdir: $!\n";

rotate_log("edit-log", '^(\d+)\s+(\d+)\s+(\S+)\s+(\S)+\s+(\S+)', 'moin');
rotate_log("event-log", '^(\d{0,16})\s+(\S+)\s+(\S+)', 'moin');

chdir $local_logdir or die "Can't cd to $local_logdir: $!\n";

rotate_log("account-creation-check", '^(\d\d\d\d-\d\d-\d\d\ \d\d:\d\d:\d\d)', 'local');

$gid = get_gid_by_name("wikiadm");
rotate_log("kill-spammer", '^(\d\d\d\d-\d\d-\d\d\ \d\d:\d\d:\d\d)', 'local');

exit 0;
