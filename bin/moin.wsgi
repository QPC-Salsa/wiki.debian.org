# vim: set ft=python ts=4 sw=4 et sm si:
import sys
sys.path.insert(0, '/srv/wiki.debian.org/etc/moin')
from MoinMoin import log
log.load_config('/srv/wiki.debian.org/etc/moin/log')
from MoinMoin.web.serving import make_application
application = make_application(shared=True)
