#!/usr/bin/env python
# GNU GPL-3+, 2011, Paul Tagliamonte <tag@pault.ag>
#  Launchpad bug gateway

import os
import sys
import hashlib
import httplib2
import traceback
import lazr.restfulclient._browser
from launchpadlib.launchpad import Launchpad
from lazr.restfulclient.errors import NotFound
import cgi
try:
	import json
except ImportError:
	import simplejson as json

__version__ = '0.2'
cachedir	= '/srv/wiki.debian.org/var/launchpad'
errordir = os.path.join(cachedir, 'errors')
defaultDist = 'ubuntu'
prettyPrint = True
ca_cert = '/etc/ssl/ca-global/ca-certificates.crt'
if os.path.exists(ca_cert):
  lazr.restfulclient._browser.SYSTEM_CA_CERTS = ca_cert
  httplib2.CA_CERTS = ca_cert

try:
	launchpad = Launchpad.login_anonymously('debian-wiki', 'production', cachedir)
except:
	import os
	from glob import glob
	aglob = 'api.launchpad.net,1.0,-application,*,*'
	paths = glob(os.path.join(cachedir, 'api.launchpad.net', 'cache', aglob))
	for path in paths: os.remove(path)
	launchpad = Launchpad.login_anonymously('debian-wiki', 'production', cachedir)

def application(environ, start_response):
	arguments = cgi.FieldStorage(fp=environ['wsgi.input'], environ=environ)
	try:
		payload = {}
		payload['cgi-status'] = 'incomplete'
		payload['cgi-version'] = __version__
		payload['descr'] = 'Unknown Status'

		eyedee = arguments['bug'].value
		bug = launchpad.bugs[eyedee]
		payload['id']	 = bug.id
		payload['title']  = bug.title

		# We'll dump the first attr in, or if we have a
		# default dist, we'll use that.
		payload['status']	 = None
		payload['importance'] = None
		payload['complete']   = None
		payload['si-dist']	= None

		distStatus = {}
		payload['dist-status'] = distStatus

		for target in bug.bug_tasks:
			try:
				distInf = {}
				distID  = target.target.name
				distInf['status']	 = target.status
				distInf['importance'] = target.importance
				distInf['complete']   = target.is_complete

				if payload['status'] == None or distID == defaultDist:
					# We're only checking status, since we'll write them out
					# together.
					payload['status']	 = target.status
					payload['si-dist']	= distID
					payload['importance'] = target.importance
					payload['complete']   = target.is_complete

				try:
					distInf['related_ticket'] = target.bug_watch.url
				except AttributeError, e:
					pass

				distStatus[distID] = distInf
			except NotFound, e:
				# log error? meh.
				pass

		payload['descr']	  = 'Bug loaded with success.'
		payload['url']		= bug.web_link
		payload['cgi-status'] = 'ok'
		status = '200 OK'
	except KeyError, e:
		payload['cgi-status'] = 'fail'
		payload['descr']	  = 'bad bug ID given to the bug flag.'
		payload['error']	  = 'KeyError: %s' % str(e)
		status = '404 Not Found'
	except Exception, e:
		payload['cgi-status'] = 'fail'
		payload['descr']	  = 'some sort of crash in launchpadlib.'
		payload['error']	  = 'Error: %s' % str(e)
		status = '500 Internal Server Error'
		exc_type, exc_value, exc_traceback = sys.exc_info()
		error = ''.join(traceback.format_exception(exc_type, exc_value, exc_traceback))
		try: eyedee = int(eyedee)
		except:
			eyedee = 'badbug'
			error = '\n'.join('Invalid bug value:', eyedee, error)
		errorfile = os.path.join(errordir, '%s-%s' % (eyedee, hashlib.md5(error).hexdigest()))
		if not os.path.exists(errorfile):
			with open(errorfile, 'w') as f:
				f.write(error)

	if not prettyPrint:
		output = json.dumps(payload)
	else:
		output = json.dumps(payload, sort_keys = True, indent = 4)

	response_headers = [('Content-type', 'application/json'), ('Content-Length', str(len(output)))]
	start_response(status, response_headers)

	return [output]
